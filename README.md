A simple solution for a diffusion problem, made to learn MPI. 

Run it with
``` 
./go.sh
```
This will also use `gnuplot` for displaying the results.
