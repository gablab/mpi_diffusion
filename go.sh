#dbflag=#"-O0 -g"

fcompiler="mpifort"

mkdir -p output
rm -f output/*
rm -f a.out; $fcompiler $dbflag solve_diff_avg.f90 mpi_diffusion.f03 main.f03; mpirun --use-hwthread-cpus -n 4 ./a.out
echo "set pm3d map; do for [i=0:10000:600] {spl 'output/OUTPAR_'.sprintf('%.8i',i).'.dat' title sprintf('%.8i',i); pause .2}" | gnuplot -p

