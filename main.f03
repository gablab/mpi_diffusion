! solve diffusion equation
program main
  use mpi_diffusion
  implicit none
  integer, parameter :: itmax = 10000 !50000

  integer :: i
  integer :: tmpint
  real :: e,etot, t(2)


  e = dtime( t )
  !call init(5,5)
  call init(50,50)
  call mpi_comm_rank(mpi_comm_cartesian, crank, ierror)
  !print*,"initialized",rank


  call gather_phi()
  if(crank.eq.0) call print_viz()

  do it=1,itmax
    if(mod(it,itmax/20).eq.0 .and. rank.eq.0) write(*,*) it,"/",itmax
    call step()
    if(mod(it,itmax/50).eq.0) then
      call gather_phi()
      if(crank.eq.0) then 
        call print_viz()
      end if
    end if
    if(mod(it,itmax/20).eq.0 .and. rank.eq.0) then
      e = dtime( t )
      etot = etot + e
      print *, 'elapsed:', e, ', user:', t(1), ', sys:', t(2)
    end if
  end do

  call terminate()
  e = dtime(t)
  etot = etot + e 
  print *, 'elapsed:', etot


end program
